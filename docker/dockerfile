FROM composer AS composer
FROM node AS node
FROM php:8.1-apache AS apache

# ------- COPY DEPENDENCIES ------- 
COPY --from=composer /usr/bin/composer /usr/local/bin/composer
COPY --from=node /usr/local /usr/local
# ------- COPY DEPENDENCIES ------- 

RUN apt update -y \
    && apt install pip git zip -y \
    && apt-get install -y wget gnupg \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*


COPY ./docker/nginx/default.conf /etc/apache2/sites-available/000-default.conf


ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions exif sodium pdo_mysql zip intl

WORKDIR /var/www/

COPY . .

RUN composer install && \
    npm install --force && \
    npm run build
    
EXPOSE 80